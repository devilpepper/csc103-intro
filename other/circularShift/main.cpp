#include <iostream>
using std::cin;
using std::cout; 
using std::endl;
#include <string>
using std::string;

void circularShift(string &s, size_t n)
{
	int sL = s.length(), shifts = 1;
	char temp, temp2;
	if(sL%n == 0) shifts = n;
	temp = s[i];
	for(int i=0; i<shifts; i++)
	{
		temp = s[i];
		for(int j=0; j<sL; j+=n)
		{
			temp2 = s[((n*j)+i+n)%sL];
			s[((n*j)+n+i)%sL] = temp;
			temp = temp2;
		}
	}
}


int main()
{
	string text[4] = {"mad", "stuff", "hadouken", "programming"};
	string s[4];
	for(int i=0; i<4; i++) s[i]=text[i];
//	for(int j=0; j<5; j++)
//	{
	for(int i=0; i<4; i++)
	{
		cout<<s[i]<<endl;
		circularShift(s[i], i);
		cout<<s[i]<<endl;
		s[i] = text[i];
	}
//	}
	return 0;
}
